const GRAPHQL_QUERY = `
query($groupName: ID!, $labels: [String!]!) {
  group(fullPath: $groupName) {
    mergeRequests(
      state: opened
      labels: $labels
      includeSubgroups: true
      sort: UPDATED_ASC
    ) {
      count
      nodes {
        createdAt
        updatedAt
        project {
          nameWithNamespace
        }
        webUrl
        title
        diffStats {
          path
          additions
          deletions
        }
        conflicts
        mergeWhenPipelineSucceeds
        labels {
          count
          nodes {
            color
            textColor
            title
          }
        }
        headPipeline {
          failedJobs: jobs(statuses: FAILED) {
            nodes {
              name
            }
          }
          status
        }
        commenters {
          nodes {
            ...UserFragment
          }
        }
        reviewers {
          nodes {
            ...UserFragment
          }
        }
      }
    }
  }
}

fragment UserFragment on User {
  username
  name
  avatarUrl
}
`

export function loadMergeRequests({
                                    apiHost = 'https://gitlab.com/api/graphql',
                                    groupName = 'gitlab-org',
                                    labels = ['workflow::ready for review', 'Community contribution']
} = {}) {
  return fetch(apiHost, {
    mode: 'cors',
    method: 'post',
    body: JSON.stringify({
      query: GRAPHQL_QUERY,
      variables: { groupName, labels }
    }),
    headers: {
      'Content-Type': 'application/json'
    }
  }).then(response => response.json())
}
